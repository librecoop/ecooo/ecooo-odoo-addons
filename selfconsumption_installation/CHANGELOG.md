# Changelog

All notable changes to this project will be documented in this file (from 13.0.1.1.0 onwards).

## [13.0.1.3.0] - 2025-02-07

### 🚀 Features

- Added fields for third and fourth milestone payment

### ⚙️ Miscellaneous Tasks

- Bump version to 13.0.1.3.0 and updated changelog

## [13.0.1.2.1] - 2025-02-04

### 🐛 Bug Fixes

- Corrected typo in spanish translations
- Removed unnecessary field 'execution_date'
- Incorrect call to class method in associated_supply

### ⚙️ Miscellaneous Tasks

- Bump version to 13.0.1.2.1 and updated changelog

## [13.0.1.2.0] - 2024-11-22

### 🚀 Features

- Added new IRVE tab to selfconsumption installation
- Added synchronization between irve dates and calendar events
- Installation event name is now computed
- Added compute method to irve_scheduled
- Added participants tab with main fields
- Added link between participants and partners
- Updated translations of associated supplies

### 🐛 Bug Fixes

- Corrected field lables and added missing values

### 🚜 Refactor

- Moved every selfconsumption model to its own file

### 📚 Documentation

- Added cliff.toml for automatic changelog generation

### 🎨 Styling

- Updated indenting in selfconsumption_installation

### ⚙️ Miscellaneous Tasks

- Added exception to pyproject.toml for correct build
- Bump version to 13.0.1.2.0 and update changelog

## [13.0.1.1.1] - 2024-10-18

### 🐛 Bug Fixes

- Corrected various fields from selfconsumption installation model

### ⚙️ Miscellaneous Tasks

- Bump selfconsumption-installation to 13.0.1.1.1

## [13.0.1.1.0] - 2024-09-13

### 🚀 Features

- Added new button to go to contact details from associated contacts
- Added fields to installation model (zero_injection & supply_connection)
- Added maintenance_contact field to installation model

### 🐛 Bug Fixes

- Linked correct values between inverter, modules and installations
- Various ui fixes
- Changed approval_number type from Integer to Char

### 🧪 Testing

- Test package pipeline

<!-- generated by git-cliff -->
