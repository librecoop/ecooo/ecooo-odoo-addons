# Ecooo Odoo Addons
Collection of Odoo addons to extend Odoo's capabilities to fit Ecooo's requirements. Each subfolder of this repo represents it's own module. Most of this modules depend on other modules developed by Domatix so they are not independent.

## Post-install

1. Create an API Key (Settings -> Technical -> Auth Api Key) for an admin user

2. Create a JWT Validator (Settings -> Users & Companies -> JWT Validators) with name 'validator' and User Id Strategy 'User ID'


## Changelog generation with [Git Cliff](https://git-cliff.org/docs/) (must have git-cliff installed)

To generate the changelog of an specific module first `cd` into the module and then execute:

```sh
git cliff -o CHANGELOG.md --include-path "<NAME-OF-FOLDER>/*" --repository "../" 
```
