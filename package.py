import os
from setuptools_odoo.git_postversion import get_git_postversion, STRATEGY_P1_DEVN
from setuptools_odoo.manifest import NoManifestFound

for dir in os.listdir('.'):
    try:
        print(dir, get_git_postversion(dir, STRATEGY_P1_DEVN))

        repository, password = 'testpypi', os.environ.get('API_TOKEN_TEST')
        if os.environ.get('CI_COMMIT_TAG') == f'{dir}-{get_git_postversion(dir, STRATEGY_P1_DEVN)}':
            repository, password = 'pypi', os.environ.get('API_TOKEN_PROD')
            print('main repository')

        os.system(f'''cd {dir} &&
                      python -m build &&
                      twine upload -r {repository} -u __token__ -p {password} --non-interactive dist/*''')
    except NoManifestFound:
        pass
